<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;

class SoapServices
{
    use ConsumesExternalService;

    public $base_uri;
    public $secret;

    public function __construct()
    {
        $this->base_uri = config('services.soapService.base_uri');
        $this->secret = config('services.soapService.secret');
    }

    public function operationsClients($xml)
    {
        return $this->perfomRequest('POST', 'user', ['SOAPAction' => '','Content-Type' => 'text/xml'], $xml);
    }

    public function operationsWallets($xml)
    {
        return $this->perfomRequest('POST', 'whallet', ['SOAPAction' => '','Content-Type' => 'text/xml'], $xml);
    }

    public function operationsPayments($xml)
    {
        return $this->perfomRequest('POST', 'payment', ['SOAPAction' => '','Content-Type' => 'text/xml'], $xml);
    }
}
