<?php

namespace App\Http\Controllers;

use App\Services\SoapServices;
use App\Traits\ConsumesExternalService;
use DOMDocument;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as Psr7Request;
use Illuminate\Http\Request;
use SimpleXMLElement;
use XMLWriter;

class WalletController extends Controller
{
    use ConsumesExternalService;

    public $soapServices;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SoapServices $soapServices)
    {
        $this->soapServices = $soapServices;
    }

    public function store(Request $request)
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->writeElement('amount',     $request->get('amount'));
        $writer->writeElement('document',   $request->get('document'));
        $writer->writeElement('phone',      $request->get('phone'));

        $xml = '
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
            <soapenv:Header/>
            <soapenv:Body>
                <insertWallet soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                    <inputsWallet xsi:type="wal:inputsWallet" xmlns:wal="http://backendsoap.test/soap/WalletsService">
                        <!--You may enter the following 3 items in any order-->
                        '.$writer->outputMemory().'
                    </inputsWallet>
                </insertWallet>
            </soapenv:Body>
        </soapenv:Envelope>
        ';

        $respuesta = $this->soapServices->operationsWallets($xml);

        $respuesta = new SimpleXMLElement($respuesta);
        $cuerpo = $respuesta->children('SOAP-ENV', true)->Body->children('ns1', true)->insertWalletResponse->children('',true)->outputWallet;
        $res = json_decode(json_encode($cuerpo));

        return response()->json($res);
    }

    public function balance(Request $request)
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->writeElement('document',   $request->get('document'));
        $writer->writeElement('phone',      $request->get('phone'));

        $xml = '
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
            <soapenv:Header/>
            <soapenv:Body>
                <balanceWallet soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                    <inputsBalance xsi:type="wal:inputsBalance" xmlns:wal="http://backendsoap.test/soap/WalletsService">
                        <!--You may enter the following 2 items in any order-->
                        '.$writer->outputMemory().'
                    </inputsBalance>
                </balanceWallet>
            </soapenv:Body>
        </soapenv:Envelope>
        ';

        $respuesta = $this->soapServices->operationsWallets($xml);

        $respuesta = new SimpleXMLElement($respuesta);
        $cuerpo = $respuesta->children('SOAP-ENV', true)->Body->children('ns1', true)->balanceWalletResponse->children('',true)->outputBalance;
        $res = json_decode(json_encode($cuerpo));

        return response()->json($res);
    }

}
