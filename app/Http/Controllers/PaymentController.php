<?php

namespace App\Http\Controllers;

use App\Services\SoapServices;
use App\Traits\ConsumesExternalService;
use Exception;
use Illuminate\Http\Request;
use SimpleXMLElement;
use XMLWriter;

class PaymentController extends Controller
{
    use ConsumesExternalService;

    public $soapServices;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SoapServices $soapServices)
    {
        $this->soapServices = $soapServices;
    }

    public function store(Request $request)
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->writeElement('client',             $request->get('client'));
        $writer->writeElement('amount',             $request->get('amount'));
        $writer->writeElement('dataTypeToClient',   $request->get('dataTypeToClient'));
        $writer->writeElement('payToClient',        $request->get('payToClient'));

        $xml = '
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
            <soapenv:Header/>
            <soapenv:Body>
                <insertPay soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                    <inputsPay xsi:type="pay:inputsPay" xmlns:pay="http://backendsoap.test/soap/PaymentsService">
                        <!--You may enter the following 4 items in any order-->
                        '.$writer->outputMemory().'
                    </inputsPay>
                </insertPay>
            </soapenv:Body>
        </soapenv:Envelope>
        ';

        $respuesta = $this->soapServices->operationsPayments($xml);

        $respuesta = new SimpleXMLElement($respuesta);
        $cuerpo = $respuesta->children('SOAP-ENV', true)->Body->children('ns1', true)->insertPayResponse->children('',true)->outputPay;
        $res = json_decode(json_encode($cuerpo));

        return response()->json($res);
    }

    public function processedPayment(Request $request)
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->writeElement('client', $request->get('client'));
        $writer->writeElement('code',   $request->get('code'));

        $xml = '
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
            <soapenv:Header/>
            <soapenv:Body>
                <processedPay soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                    <inputsProcessedPay xsi:type="pay:inputsProcessedPay" xmlns:pay="http://backendsoap.test/soap/PaymentsService">
                        <!--You may enter the following 2 items in any order-->
                        '.$writer->outputMemory().'
                    </inputsProcessedPay>
                </processedPay>
            </soapenv:Body>
        </soapenv:Envelope>
        ';

        $respuesta = $this->soapServices->operationsPayments($xml);

        $respuesta = new SimpleXMLElement($respuesta);
        $cuerpo = $respuesta->children('SOAP-ENV', true)->Body->children('ns1', true)->processedPayResponse->children('',true)->outputProcessedPay;
        $res = json_decode(json_encode($cuerpo));

        return response()->json($res);
    }


}
