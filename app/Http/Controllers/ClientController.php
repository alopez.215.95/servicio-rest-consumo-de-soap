<?php

namespace App\Http\Controllers;

use App\Services\SoapServices;
use App\Traits\ConsumesExternalService;
use Exception;
use Illuminate\Http\Request;
use SimpleXMLElement;
use XMLWriter;

class ClientController extends Controller
{
    use ConsumesExternalService;

    public $soapServices;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SoapServices $soapServices)
    {
        $this->soapServices = $soapServices;
    }


    public function store(Request $request)
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->writeElement('name',       $request->get('name'));
        $writer->writeElement('email',      $request->get('email'));
        $writer->writeElement('document',   $request->get('document'));
        $writer->writeElement('phone',      $request->get('phone'));

        $xml = '
        <soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
            <soapenv:Header/>
            <soapenv:Body>
                <insertUser soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
                    <inputsUser xsi:type="user:inputsUser" xmlns:user="http://backendsoap.test/soap/UsersService">
                        <!--You may enter the following 4 items in any order-->
                        '.$writer->outputMemory().'
                    </inputsUser>
                </insertUser>
            </soapenv:Body>
        </soapenv:Envelope>
        ';

        $respuesta = $this->soapServices->operationsClients($xml);

        $respuesta = new SimpleXMLElement($respuesta);
        $cuerpo = $respuesta->children('SOAP-ENV', true)->Body->children('ns1', true)->insertUserResponse->children('',true)->outputUser;
        $res = json_decode(json_encode($cuerpo));

        return response()->json($res);
    }

}
