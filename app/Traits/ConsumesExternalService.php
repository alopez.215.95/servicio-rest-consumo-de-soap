<?php

namespace App\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Response;

trait ConsumesExternalService{

    /**
     * Respuesta de exito
     * @param string|array $data
     * @param int $code
     * @return Illuminate\Http\JsonResponse
     */
    public function perfomRequest($method, $baseUrl, $headers = [], $xml){

        $client = new Client([
            'base_uri' => $this->base_uri,
        ]);

        if (isset($this->secret)) {
            $headers['Authorization'] = $this->secret;
        }

        $request = new Request($method, $baseUrl, $headers, $xml);
        $res = $client->send($request);

        return $res->getBody()->getContents();
    }

}
