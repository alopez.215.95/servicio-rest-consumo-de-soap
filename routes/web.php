<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Http\Controllers\ClientController;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('client', 'ClientController@store');
$router->post('wallet-balance', 'WalletController@balance');
$router->post('wallet-recharge', 'WalletController@store');
$router->post('payment-processed', 'PaymentController@processedPayment');
$router->post('payment-pay', 'PaymentController@store');
