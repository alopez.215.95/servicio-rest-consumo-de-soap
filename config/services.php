<?php

return [
    'soapService' => [
        'base_uri' => env('SOAP_SERVICE_BASE_URI'),
        'secret' =>  env('SOAP_SERVICE_SECRET')
    ],
];
